" ------- PLUGIN SETTINGS/INITIALIZATIONS -------
execute pathogen#infect()

" settings for VIM Table Mode, enable table mode with <Leader>tm
"let g:table_mode_corner_corner='+'
"let g:table_mode_header_fillchar='='

" ------- END OF PLUGIN SETTINGS/INITIALIZATIONS -------


" ------- VARIOUS COLOR SCHEMES AND OTHER COLOR SETTINGS -------

"colorscheme angr
"colorscheme apprentice
"colorscheme archery
"colorscheme challenger_deep
"colorscheme deep-space
"colorscheme delek
"colorscheme desert
"colorscheme focuspoint
"colorscheme happy_hacking
"colorscheme hybrid
"colorscheme iceberg
colorscheme jellybeans
"colorscheme lucid
"colorscheme pink-moon
"colorscheme seoul256
"colorscheme sierra
"colorscheme space-vim-dark

" syntax hilight
syntax on
" Special coloring for characters exceeding col 80
"highlight OverLength guifg=black guibg=white
"match OverLength /\%81v.\+/

" ------- END OF VARIOUS COLOR SCHEMES AND OTHER COLOR SETTINGS -------


" ------- MISCELLANEOUS SETTINGS -------

" read a file again in the event it has changed outside vim
set autoread

" show cursor row, column and position relative to buffer
set ruler

" show horizontal menu for command completion
set wildmenu

" always show status line
set laststatus=2

" set character encoding stuff
set enc=utf8
set fileencoding=utf8

" set tab as 4 spaces, indentation with 4 spaces
set tabstop=4 shiftwidth=4 expandtab

" always show at least 10 lines above/below cursor
set scrolloff=10

"always show at least 10 columns before/after cursor
set sidescrolloff=10

" auto-indent
set ai

" ignore case when searching...
set ignorecase

" ...unless the search term contains an uppercase letter
set smartcase

" hilight search matches
set hlsearch

" jump automatically to the first match of the search
set incsearch

" relative line numbering
set relativenumber

" absolute line numbering on the current line
set number

" completeopt works by choosing the longest common text of all matches
set completeopt=longest,menuone

set backspace=eol,start,indent

" wrap to next/previous line when navigating with arrow keys or h, l
set whichwrap+=<,>,h,l

" in case fugitive plugin is loaded, show the current working branch in status line
set statusline=%{exists('g:loaded_fugitive')?fugitive#statusline():''}

" append statusline with full filepath, cursor column, current line/total lines, percentage of document
set statusline+=%F%m%=%c,%l/%L\ %P

" change the working directory to that of the file being edited
set autochdir

" prevent creating backup files
set nobackup

" prevent creating .swp files
set noswapfile

" don't assume numbers prepended with 0 to be in octal base
set nrformats-=octal

" automatically trim trailing whitespace on save. N.B.: be careful with this!
"autocmd BufWritePre * %s/\s\+$//e

" ------- END OF MISCELLANEOUS SETTINGS -------


" ------- CUSTOM FUNCTIONS BELOW -------

" declare a function for trimming whitespace
fu! TrimWhitespace()
  let l:save = winsaveview()
  %s/\s\+$//e
  call winrestview(l:save)
endfunction

" ------- END OF CUSTOM FUNCTIONS -------


" ------- KEY MAPPINGS FOR VARIOUS PLUGINS/CUSTOM BINDINGS BELOW -------

" if you need to actually type any of the insert mode mappings below,
" insert ^V between the characters, example: II becomes I^VI
" (within insert mode) jump to the first non-blank character of the line
"inoremap II <Esc>I
" (within insert mode) jump to EOL
"inoremap AA <Esc>A
" (within insert mode) jump to newline above the current line
"inoremap OO <Esc>O

" (within insert mode) change line from the current cursor position to EOL
"inoremap CC <Esc>C
" (within insert mode) change the whole current line
"inoremap SS <Esc>S
" (within insert mode) delete the whole current line
"inoremap DD <Esc>dd
" (within insert mode) undo
"inoremap UU <Esc>u

" Get rid of some of the (many) antipatterns
noremap <Left> <NOP>
noremap <Right> <NOP>
noremap <Up> <NOP>
noremap <Down> <NOP>

" Having a slow day? Challenge yourself!
"noremap h <NOP>
"noremap j <NOP>
"noremap k <NOP>
"noremap l <NOP>

" Navigate between multiple windows using ALT + direction
nmap <silent> <A-k> :wincmd k<CR>
nmap <silent> <A-j> :wincmd j<CR>
nmap <silent> <A-h> :wincmd h<CR>
nmap <silent> <A-l> :wincmd l<CR>

" insert an empty line below the current line
nnoremap <silent> <c-j> :silent call append('.', '')<cr>

" insert an empty line above the current line
nnoremap <silent> <c-k> :silent call append(line('.')-1, '')<cr>

" toggle the use of relative line numbers
"nnoremap <F3> :NumbersToggle<CR>

" toggle the use of line numbers
"nnoremap <F4> :NumbersOnOff<CR>

" bind TrimWhitespace() to F5
noremap <F5> :call TrimWhitespace()<CR>

" map the toggling of NERDTree to F6
"nmap <F6> :NERDTreeToggle<CR>

" ^L to clear highlighting set by :set hlsearch
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

" allow color schemes to use bright colors without forcing bold
if &t_Co == 8 && $TERM !~# '^linux\|^Eterm'
  set t_Co=16
endif

" -------  END OF KEY MAPPINGS -------
